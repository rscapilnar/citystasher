import React, { Component } from "react";
import { Button, Icon, Form, Dropdown } from "semantic-ui-react";
import "./StashPointsForm.css";

import * as actions from "../../actions/";

import { connect } from "react-redux";

class StashPointsForm extends Component {
  constructor(props) {
    super(props);
    this.sortOptions = [
      {
        text: "By Capacity",
        value: "by_capacity"
      },
      {
        text: "By Bags in last 30 days",
        value: "by_bags_last_30_days"
      }
    ];
    this.sortOrderOptions = [
      {
        text: "Ascending",
        value: "asc"
      },
      {
        text: "Descending",
        value: "desc"
      }
    ];
    this.state = {
      active: true,
      featured: false,
      twentyfour_seven: false,
      open_late: false,
      sort: null,
      city: null,
      sort_order: null
    };
  }

  toggleButton = event => {
    event.preventDefault();
    this.setState({
      [event.target.id]: !this.state[event.target.id]
    });
  };

  setDropdownValue = (event, { id, value }) => {
    this.setState({
      [id]: value
    });
  };

  getStashPoints = () => {
    let qs = `?${this.state.active ? "active=true" : ""}${
      this.state.featured ? "&featured=true" : ""
    }${this.state.twentyfour_seven ? "&twentyfour_seven=true" : ""}${
      this.state.open_late ? "&open_late=true" : ""
    }`;

    if (this.state.sort) {
      qs += `&${this.state.sort}=${this.state.sort_order}`;
    }
    if (this.state.city) {
      qs += `&city=${this.state.city}`;
    }

    this.setLoading();
    this.props.getStashPoints(qs, this.setLoading);
  };

  setLoading = () => {
    this.setState({ stashPointsLoading: !this.state.stashPointsLoading });
  };

  render() {
    return (
      <div>
        <Form size="tiny">
          <Form.Group inline>
            <div className="fields-wrapper">
              <Form.Field
                className="city-input"
                control="input"
                placeholder="City"
                onChange={event => {
                  this.setState({ city: event.target.value });
                }}
              />
              <Button
                size="tiny"
                toggle
                icon
                labelPosition="left"
                id="active"
                active={this.state.active}
                onClick={this.toggleButton}
              >
                <Icon name={this.state.active ? "check" : "minus"} />
                Active
              </Button>
              <Button
                size="tiny"
                toggle
                icon
                labelPosition="left"
                id="twentyfour_seven"
                active={this.state.twentyfour_seven}
                onClick={this.toggleButton}
              >
                <Icon name={this.state.twentyfour_seven ? "check" : "minus"} />
                24/7
              </Button>
              <Button
                size="tiny"
                toggle
                icon
                labelPosition="left"
                id="open_late"
                active={this.state.open_late}
                onClick={this.toggleButton}
              >
                <Icon name={this.state.open_late ? "check" : "minus"} />
                Open Late
              </Button>
              <Button
                size="tiny"
                toggle
                icon
                labelPosition="left"
                id="featured"
                active={this.state.featured}
                onClick={this.toggleButton}
              >
                <Icon name={this.state.featured ? "check" : "minus"} />
                Featured
              </Button>
              <Dropdown
                id="sort"
                onChange={this.setDropdownValue}
                placeholder="Order By"
                selection
                options={this.sortOptions}
              />
              <Dropdown
                id="sort_order"
                onChange={this.setDropdownValue}
                placeholder="Direction"
                selection
                options={this.sortOrderOptions}
              />
              <Button
                color="violet"
                loading={this.state.stashPointsLoading ? true : false}
                className="submit-button"
                style={{ float: "right" }}
                onClick={() => this.getStashPoints()}
              >
                Get StashPoints
              </Button>
            </div>
          </Form.Group>
        </Form>
      </div>
    );
  }
}

export default connect(
  null,
  actions
)(StashPointsForm);
