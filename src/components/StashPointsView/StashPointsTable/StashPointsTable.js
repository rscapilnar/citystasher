import React, { Component } from "react";
import { connect } from "react-redux";
import { Table, Icon } from "semantic-ui-react";

class StashPointsTable extends Component {
  constructor(props) {
    super(props);
    this.currencyCodes = {
      GBP: "£",
      EUR: "€",
      USD: "$"
    };
  }

  renderStashPoint = stashPoint => {
    return (
      <Table.Row key={stashPoint.id}>
        <Table.Cell className="center aligned">
          <Icon
            name="circle"
            color={`${stashPoint.status === "active" ? "green" : "grey"}`}
          />
        </Table.Cell>
        <Table.Cell>{stashPoint.name}</Table.Cell>
        <Table.Cell>{stashPoint.capacity}</Table.Cell>
        <Table.Cell>{stashPoint.address}</Table.Cell>
        <Table.Cell>
          {this.currencyCodes[stashPoint.pricing_structure.ccy]
            ? this.currencyCodes[stashPoint.pricing_structure.ccy]
            : stashPoint.pricing_structure.ccy}
          {stashPoint.pricing_structure.first_day_cost / 100}/
          {this.currencyCodes[stashPoint.pricing_structure.ccy]
            ? this.currencyCodes[stashPoint.pricing_structure.ccy]
            : stashPoint.pricing_structure.ccy}
          {stashPoint.pricing_structure.extra_day_cost / 100}
        </Table.Cell>
      </Table.Row>
    );
  };

  render() {
    return (
      <Table celled padded color="violet">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={1}>Status</Table.HeaderCell>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Capacity</Table.HeaderCell>
            <Table.HeaderCell>Address</Table.HeaderCell>
            <Table.HeaderCell>First Day / Extra Day</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {this.props.stashPoints.map(this.renderStashPoint)}
        </Table.Body>
      </Table>
    );
  }
}

const mapStateToProps = state => {
  return { stashPoints: state.stashPoints };
};

export default connect(mapStateToProps)(StashPointsTable);
