import React, { Component } from "react";
import { connect } from "react-redux";
import { Card, Icon, Image, Popup, Rating } from "semantic-ui-react";
import "./StashPointsCards.css";

class StashPointsCards extends Component {
  constructor(props) {
    super(props);
    this.currencyCodes = {
      GBP: "£",
      EUR: "€",
      USD: "$"
    };
  }

  renderStashPoint = stashPoint => {
    const capacity = (
      <span>
        <Icon name="suitcase" />
        {stashPoint.capacity}
      </span>
    );
    const prices = (
      <div className="right floated">
        {this.currencyCodes[stashPoint.pricing_structure.ccy]
          ? this.currencyCodes[stashPoint.pricing_structure.ccy]
          : stashPoint.pricing_structure.ccy}
        {stashPoint.pricing_structure.first_day_cost / 100}/
        {this.currencyCodes[stashPoint.pricing_structure.ccy]
          ? this.currencyCodes[stashPoint.pricing_structure.ccy]
          : stashPoint.pricing_structure.ccy}
        {stashPoint.pricing_structure.extra_day_cost / 100}
      </div>
    );
    const mapLink = (
      <a
        href={`https://www.google.com/maps/@${stashPoint.latitude},${
          stashPoint.longitude
        },18z`}
        target="_blank"
        className="map-link"
      >
        <Icon name="map" />
      </a>
    );
    return (
      <Card color="violet" key={stashPoint.id}>
        <Image src={stashPoint.photos[0]} />
        <Card.Content>
          <Card.Header>{stashPoint.name}</Card.Header>
          <Card.Meta>
            {stashPoint.address}, {stashPoint.postal_code}
            <br />
            {stashPoint.rating ? (
              <Rating
                defaultRating={stashPoint.rating}
                maxRating={5}
                disabled
              />
            ) : (
              ""
            )}
          </Card.Meta>
          <Card.Description>{stashPoint.description}</Card.Description>
        </Card.Content>
        <Card.Content extra>
          {stashPoint.featured ? (
            <Popup
              position="top center"
              size="mini"
              trigger={<Icon name="star" color="yellow" />}
              inverted
              content="Featured"
            />
          ) : (
            ""
          )}
          <Popup
            position="top center"
            size="mini"
            trigger={
              <Icon
                name="circle"
                color={`${stashPoint.status === "active" ? "green" : "grey"}`}
              />
            }
            inverted
            content="Active"
          />
          <Popup
            position="top center"
            size="mini"
            trigger={capacity}
            inverted
            content="Capacity"
          />
          <Popup
            position="top center"
            size="mini"
            trigger={mapLink}
            inverted
            content="See on map"
          />
          <Popup
            position="top center"
            size="mini"
            trigger={prices}
            inverted
            content="First / Extra Days"
          />
        </Card.Content>
      </Card>
    );
  };

  render() {
    return (
      <div>
        <Card.Group>
          {this.props.stashPoints.map(this.renderStashPoint)}
        </Card.Group>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    stashPoints: state.stashPoints
  };
};

export default connect(mapStateToProps)(StashPointsCards);
