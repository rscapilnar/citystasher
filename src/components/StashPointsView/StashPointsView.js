import React, { Component } from "react";
import { connect } from "react-redux";
import StashPointsTable from "./StashPointsTable/StashPointsTable";
import StashPointsCards from "./StashPointsCards/StashPointsCards";
import { Button, Icon, Divider } from "semantic-ui-react";

class StashPointsView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      view: "table"
    };
  }

  changeView = (event, type) => {
    event.preventDefault();
    this.setState({
      view: type
    });
  };

  render() {
    if (this.props.stashPoints.length > 0) {
      return (
        <div>
          <div style={{ padding: "10px" }}>
            <span className="left floated">
              Found {this.props.stashPoints.length} Stash Points
            </span>
            <Button.Group className="right floated" icon>
              <Button
                active={this.state.view === "table"}
                onClick={e => this.changeView(e, "table")}
              >
                <Icon name="table" />
              </Button>
              <Button
                active={this.state.view === "cards"}
                onClick={e => this.changeView(e, "cards")}
              >
                <Icon name="address card" />
              </Button>
            </Button.Group>
          </div>
          <Divider />
          {this.state.view === "table" ? (
            <StashPointsTable />
          ) : (
            <StashPointsCards />
          )}
        </div>
      );
    } else {
      return (
        <h4>
          No StashPoints to show. Use the filters above to refine your search.
        </h4>
      );
    }
  }
}

const mapStateToProps = state => {
  return { stashPoints: state.stashPoints };
};

export default connect(mapStateToProps)(StashPointsView);
