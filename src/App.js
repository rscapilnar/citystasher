import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid } from "semantic-ui-react";

import StashPointsForm from "./components/StashPointsForm/StashPointsForm";
import StashPointsView from "./components/StashPointsView/StashPointsView";

import * as actions from "./actions";

class App extends Component {
  getStashPoints = () => {
    this.props.getStashPoints("?featured=true");
  };

  render() {
    return (
      <div className="App">
        <Grid container columns="equal">
          <Grid.Row>
            <Grid.Column>
              <StashPointsForm />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <StashPointsView />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default connect(
  null,
  actions
)(App);
