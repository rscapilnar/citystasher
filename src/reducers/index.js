import { combineReducers } from "redux";
import StashPointsReducer from "./reduce_stashpoints";

const rootReducer = combineReducers({
  stashPoints: StashPointsReducer
});

export default rootReducer;
