import { RETRIEVE_STASH_POINTS } from "../actions/types";

export default function(state = [], action) {
  switch (action.type) {
    case RETRIEVE_STASH_POINTS:
      return action.payload;
    default:
      return state;
  }
}
