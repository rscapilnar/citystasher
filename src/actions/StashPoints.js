import { RETRIEVE_STASH_POINTS } from "./types";

import { api } from "../config/api";

export const getStashPoints = (params, cb) => {
  return function(dispatch) {
    api
      .get(`/stashpoints${params}`)
      .then(response => {
        cb();
        dispatch({
          type: RETRIEVE_STASH_POINTS,
          payload: response.data
        });
      })
      .catch(err => {
        cb();
        console.error(err);
      });
  };
};
